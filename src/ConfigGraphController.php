<?php

/**
 * @file
 * Contains \Drupal\config_graph\ConfigGraphController.
 */

namespace Drupal\config_graph;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConfigGraphController extends ControllerBase {

  protected $grapher;

  /**
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   */
  public function __construct(Grapher $grapher) {
    $this->grapher = $grapher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config_graph.grapher')
    );
  }

  public function downloadGraph() {
  }

}
