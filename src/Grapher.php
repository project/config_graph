<?php

/**
 * @file
 * Contains \Drupal\config_graph\Grapher.
 */

namespace Drupal\config_graph;

use Drupal\Core\Config\Entity\ConfigEntityDependency;
use Drupal\Core\Config\StorageInterface;
use phpDocumentor\GraphViz\Edge;
use phpDocumentor\GraphViz\Graph;
use phpDocumentor\GraphViz\Node;

class Grapher {

  protected $configStorage;

  /**
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   */
  public function __construct(StorageInterface $config_storage) {
    $this->configStorage = $config_storage;
  }

  /**
   * @return \phpDocumentor\GraphViz\Graph
   */
  public function createGraph() {
    $graph = new Graph();
    // This uses the configuration storage directly to avoid blowing the static
    // caches in the configuration factory and the configuration entity system.
    // Additionally this ensures that configuration entity dependency discovery
    // has no dependencies on the config entity classes. Assume data with UUID
    // is a config entity. Only configuration entities can be depended on so we
    // can ignore everything else.
    $data = array_filter($this->configStorage->readMultiple($this->configStorage->listAll()), function($config) {
      return isset($config['uuid']);
    });
    array_walk($data, function (&$config, $name) {
      $config = new ConfigEntityDependency($name, $config);
    });
    /** @var \Drupal\Core\Config\Entity\ConfigEntityDependency  $entity */
    foreach ($data as $entity) {
      $dependencies = $entity->getDependencies('config');
      // Only graph stuff with dependencies.
      if (!empty($dependencies)) {
        $base_node = new Node($entity->getConfigDependencyName());
        $graph->setNode($base_node);
        foreach ($dependencies as $dependency) {
          $dependency_node = $graph->findNode($dependency);
          if (!$dependency_node) {
            $dependency_node = new Node($dependency);
          }
          $graph->link(new Edge($base_node, $dependency_node));
        }
      }
    }
    return $graph;
  }

  public function doIt() {
    $graph = $this->createGraph();
    $graph->export('pdf', '/tmp/al.pdf');
  }
}
